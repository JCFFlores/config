set -x MANPATH /usr/share/fish/man /usr/local/man /usr/local/share/man /usr/share/man

set -x PIPENV_SHELL_FANCY 1

set -x GPG_TTY (tty)

set -g theme_nerd_fonts yes

# pyenv config
if test -d $HOME/.pyenv
    set -Ux PYENV_ROOT $HOME/.pyenv
    set -x fish_user_paths $PYENV_ROOT/bin $fish_user_paths
end

# poetry config
if test -d $HOME/.poetry
    set -x PATH $HOME/.poetry/bin $PATH
end

# Emacs ansi-term support
if test -n "$EMACS"
    set -x TERM eterm-color
end

# cargo binaries
if test -d $HOME/.cargo
    set -x PATH $HOME/.cargo/bin $PATH
end

# This function may be required
function fish_title
    true
end

# pyenv configuration
if command -s pyenv >/dev/null 2>&1
    status is-login; and pyenv init --path | source
    pyenv init - | source
end

# vterm configuration
function vterm_printf;
    if begin; [  -n "$TMUX" ]  ; and  string match -q -r "screen|tmux" "$TERM"; end
        # tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$argv"
    else if string match -q -- "screen*" "$TERM"
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$argv"
    else
        printf "\e]%s\e\\" "$argv"
    end
end

if [ "$INSIDE_EMACS" = 'vterm' ]
    function clear
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    end
end

# Abbreviations
abbr pi "ping google.com"
abbr pa "paru -S"
abbr h "htop"
abbr xq "xbps-query -Rs"
abbr xi "sudo xbps-install -S"
abbr xr "sudo xbps-remove"
abbr xu "sudo xbps-install -Su"
